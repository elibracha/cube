﻿import React from 'react';
import { Menu, Container, Button } from "semantic-ui-react";
import { Link } from 'react-router-dom';

const NavBar = () => {
    return (
        <Container>
            <Menu >
                <Menu.Item>
                    <img alt='logo' src='../../assets/logo.png' style={{ marginRight: "10px" }} />
                    Splinter
                </Menu.Item>
                <Menu.Item as={Link} exact='true' to='/projects/form/java' >
                    <Button style={{ backgroundColor: 'rgb(240, 240, 235)'}} content="Create Project" />
                </Menu.Item>
                <Menu.Item position='right' name='sign in' /> 
                <Menu.Item name='sign up' />
            </Menu>
        </Container>
    )
}

export default NavBar