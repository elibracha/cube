﻿import React, { useContext, useState, FormEvent } from 'react';
import { Form, Input, Button, Segment, Grid } from 'semantic-ui-react';
import WizardStore from '../../../stores/WizardStore';
import { observer } from 'mobx-react-lite';
import { RouteComponentProps } from 'react-router-dom';
import { genToken, goToWizard } from '../../util/utilities';
import Steps from '../../../models/Steps';

const GitForm: React.FC<RouteComponentProps> = ({ history }) => {

    const wizardStore = useContext(WizardStore)
    const { submittingStep, currentStep, stepBack, setGitWizardConfig } = wizardStore

    const [gitConfig, setGitConfig] = useState({
        url: '',
        username: '',
        password: '',
        token: '',
        projectName: ''
    })

    const handleChange = (event: FormEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        const { name, value } = event.currentTarget
        setGitConfig({ ...gitConfig, [name]: value })
    }

    const generateToken = () => {
        setGitConfig({ ...gitConfig, token: genToken() })
    }

    if (currentStep !== Steps.Git) {
        history.push(goToWizard(currentStep)!)
    }

    return (
        <Form loading={submittingStep}>
            <Form.Group widths='equal'>
                <Form.Field
                    label='Project Name'
                    onChange={handleChange}
                    control={Input}
                    placeholder='project name'
                />
                <Form.Field
                    label='BitBucket URL'
                    onChange={handleChange}
                    control={Input}
                    placeholder='URL'
                />
            </Form.Group>
            <Form.Group widths='equal'>
                <Form.Field
                    label='User Name'
                    onChange={handleChange}
                    control={Input}
                    placeholder='user name'
                />
                <Form.Field
                    label='Password'
                    onChange={handleChange}
                    control={Input}
                    type='password'
                    placeholder='password'
                />
            </Form.Group>
            <Form.Group widths='3'>
                <Form.Field
                    name='token'
                    label='Hook Token'
                    onChange={handleChange}
                    control={Input}
                    type='input'
                    value={gitConfig.token}
                    placeholder='token'
                />
                <Segment basic style={{ margin: '0.8rem 0'}}>
                    <Form.Field
                        onClick={() => generateToken()}
                        style={{ backgroundColor: 'rgb(240, 240, 235)' }}
                        control={Button}
                        content='Generate'
                    />
                </Segment>
            </Form.Group>
            <Grid container centered>
                <Grid.Column width={2} style={{ margin: '2% 0 2% 0' }}>
                    <Button
                        onClick={() => stepBack()}
                        content='Back'
                    />
                </Grid.Column>
                <Grid.Column width={2} style={{ margin: '2% 0 2% 0' }}>
                    <Button
                        style={{ backgroundColor: 'rgb(194, 70, 238)', color: 'white' }}
                        onClick={() => setGitWizardConfig(gitConfig)}
                        content='Next'
                    />
                </Grid.Column>
            </Grid> 
        </Form>
    )
}

export default observer(GitForm);