﻿import React, { useContext, useState, FormEvent } from 'react';
import { Form, TextArea, Input, Select, Button, Message, Grid } from 'semantic-ui-react';
import WizardStore from '../../../stores/WizardStore';
import JavaBuildTools from '../../../models/JavaBuildTools';
import JavaVersions from '../../../models/JavaVersions';
import { observer } from 'mobx-react-lite';
import { RouteComponentProps } from 'react-router';
import Steps from '../../../models/Steps';
import { goToWizard } from '../../util/utilities';

const JavaForm: React.FC<RouteComponentProps> = ({ history }) => {

    const wizardStore = useContext(WizardStore)
    const { currentStep, setJavaWizardConfig, submittingStep } = wizardStore

    const [javaConfig, setJavaConfig] = useState({
        javaVersion: 8,
        buildTool: JavaBuildTools.Maven,
        groupId: '',
        artifactName: '',
        version: '',
        description: ''
    })

    const handleChange = (event: FormEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        const { name, value } = event.currentTarget
        setJavaConfig({ ...javaConfig, [name]: value })
    }

    const getEnumOption = (e: any) => {
        const members = Object.keys(e);
        return members.filter((x) => Number.isNaN(parseInt(x, 10)))
            .map((key: any) => {
                return { key, text: key, value: e[key] };
            })
        
    }

    if (currentStep !== Steps.Java) {
        history.push(goToWizard(currentStep)!)
    }

    return (
        <Form loading={submittingStep}>
            <Form.Group widths='equal'>
                <Form.Field
                    control={Input}
                    onChange={handleChange}
                    label='Group ID'
                    placeholder='groupid'
                />
                <Form.Field
                    control={Input}
                    onChange={handleChange}
                    label='Artifact ID'
                    placeholder='artifactid'
                />
                <Form.Field
                    control={Input}
                    onChange={handleChange}
                    label='Version'
                    placeholder='version'
                />
                <Message
                    error
                    header='Account exists'
                    content='hello there great show' >
                </Message>
            </Form.Group>
            <Form.Field
                onChange={handleChange}
                control={TextArea}
                label='Description'
                placeholder='description'
            />
            <Form.Group>
                <Form.Field
                    control={Select}
                    onChange={handleChange}
                    options={getEnumOption(JavaBuildTools)}
                    label={{ children: 'Build Tool', htmlFor: 'form-select-control-build-tool' }}
                    placeholder='Build Tool'
                    search
                    searchInput={{ id: 'form-select-control-build-tool' }}
                />
                <Form.Field
                    width={4}
                    onChange={handleChange}
                    control={Select}
                    options={getEnumOption(JavaVersions)}
                    label={{ children: 'Java Version', htmlFor: 'form-select-control-java-version' }}
                    placeholder='Java Version'
                    search
                    searchInput={{ id: 'form-select-control-java-version' }}
                />
            </Form.Group>
            <Grid container centered>
                <Grid.Column width={2} style={{ margin: '2% 0 2% 0' }}>
                    <Button
                        style={{ backgroundColor: 'rgb(194, 70, 238)', color: 'white' }}
                        onClick={() => setJavaWizardConfig(javaConfig)}
                        content='Next'
                    />
                </Grid.Column>
            </Grid> 
        </Form>
    )
}

export default observer(JavaForm);