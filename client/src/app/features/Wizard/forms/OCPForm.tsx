﻿import React, { useContext, useState, FormEvent } from 'react';
import { Form, Input, Button, Label, Grid } from 'semantic-ui-react';
import WizardStore from '../../../stores/WizardStore';
import { observer } from 'mobx-react-lite';
import { RouteComponentProps } from 'react-router-dom';
import { goToWizard } from '../../util/utilities';
import Steps from '../../../models/Steps';

const OCPForm: React.FC<RouteComponentProps> = ({ history }) => {

    const wizardStore = useContext(WizardStore)
    const { submittingStep, currentStep, stepBack, setOcpWizardConfig } = wizardStore

    const [ocpConfig, setOcpConfig] = useState({
        namespace: '',
        owner: '',
        groupOwners: ''
    })

    const handleChange = (event: FormEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        const { name, value } = event.currentTarget
        setOcpConfig({ ...ocpConfig, [name]: value })
    }


    if (currentStep !== Steps.OCP) {
        history.push(goToWizard(currentStep)!)
    }

    return (
        <Form loading={submittingStep}>
            <Label
                style={{ position: 'absolute', lineHeight: '2em' }}
                color='violet'
                ribbon='right'
            >
                <b>Great! Just one more step!!</b><br />
                If somthing still dont work you can contact squad MSE.<br />
                Please have detailed description of the problem.
                </Label>
            <Form.Field
                width={7}
                label='Namespace'
                onChange={handleChange}
                control={Input}
                placeholder='namespace'
            />
            <Form.Field
                width={7}
                    label='Namespace owner'
                    onChange={handleChange}
                    control={Input}
                    placeholder='owner'
                />
            <Form.Field
                width={7}
                    label='Group owner'
                    onChange={handleChange}
                    control={Input}
                    placeholder='group owner'
            />

            <Grid container centered>
                <Grid.Column width={2} style={{ margin: '2% 0 2% 0'}}>
                    <Button
                        onClick={() => stepBack()}
                        content='Back'
                     />
                </Grid.Column>
                <Grid.Column width={2} style={{ margin: '2% 0 2% 0' }}>
                    <Button
                        style={{ backgroundColor: 'rgb(194, 70, 238)', color: 'white' }}
                        onClick={() => {
                            setOcpWizardConfig(ocpConfig)
                            history.push('/projects/form/processing')
                        }}
                        content='Finish'
                     />
                </Grid.Column>
            </Grid>
        </Form>
    )
}

export default observer(OCPForm);