﻿import React, { useContext, useState, FormEvent } from 'react';
import { Form, Input, Button, Label, Grid, Segment } from 'semantic-ui-react';
import WizardStore from '../../../stores/WizardStore';
import { observer } from 'mobx-react-lite';
import { RouteComponentProps } from 'react-router-dom';
import { goToWizard } from '../../util/utilities';
import Steps from '../../../models/Steps';

const JenkinsForm: React.FC<RouteComponentProps> = ({ history }) => {

    const wizardStore = useContext(WizardStore)
    const { submittingStep, currentStep, stepBack, setJenkinsWizardConfig } = wizardStore

    const [jenkinsConfig, setJenkinsConfig] = useState({
        url: '',
        username: '',
        password: '',
        jobName: ''
    })

    const handleChange = (event: FormEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        const { name, value } = event.currentTarget
        setJenkinsConfig({ ...jenkinsConfig, [name]: value })
    }


    if (currentStep !== Steps.Jenkins) {
        history.push(goToWizard(currentStep)!)
    }

    return (
        <Form loading={submittingStep}>
            <Label
                style={{ position: 'absolute', lineHeight: '2em' }}
                color='violet'
                ribbon='right'
            >
                <b>Great! You are almost there!!</b><br />
                You have any issuses or bugs? Please open new issuse in our dashboard.<br />
                And let as know about it.
            </Label>
                <Form.Field
                    width={8}
                    label='Job Name'
                    onChange={handleChange}
                    control={Input}
                    placeholder='job name'
                />
                <Form.Field
                    width={8}
                    label='URL'
                    onChange={handleChange}
                    control={Input}
                    placeholder='URL'
                />
            <Form.Field
                        width={8}
                        label='User Name'
                        onChange={handleChange}
                        control={Input}
                        placeholder='user name'
                    /> 
            <Form.Field
                        width={8}
                        label='Password'
                        onChange={handleChange}
                        control={Input}
                        type='password'
                        placeholder='password'
                    /> 
            <Grid container centered>
                <Grid.Column width={2} style={{ margin: '2% 0 2% 0' }}>
                    <Button
                        onClick={() => stepBack()}
                        content='Back'
                    />
                </Grid.Column>
                <Grid.Column width={2} style={{ margin: '2% 0 2% 0' }}>
                    <Button
                        style={{ backgroundColor: 'rgb(194, 70, 238)', color: 'white' }}
                        onClick={() => setJenkinsWizardConfig(jenkinsConfig)}
                        content='Next'
                    />
                </Grid.Column>
            </Grid> 
        </Form>
    )
}

export default observer(JenkinsForm);