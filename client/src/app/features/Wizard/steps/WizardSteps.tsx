﻿import React, { useContext } from 'react';
import { Step } from 'semantic-ui-react';
import WizardStore from '../../../stores/WizardStore';
import { observer } from 'mobx-react-lite';

const WizardSteps = () => {

    const wizardStore = useContext(WizardStore);
    const { currentStep } = wizardStore;

    return (
        <Step.Group size='tiny' fluid>
            <Step key={0} active={currentStep === 0} >
                <Step.Content>
                    <Step.Title>Java</Step.Title>
                </Step.Content>
            </Step>

            <Step key={1} active={currentStep === 1}>
                <Step.Content>
                    <Step.Title>Git</Step.Title>
                </Step.Content>
            </Step>

            <Step key={2} active={currentStep === 2}>
                <Step.Content>
                    <Step.Title>Jenkins</Step.Title>
                </Step.Content>
            </Step>

            <Step key={3} active={currentStep === 3}>
                <Step.Content>
                    <Step.Title>OCP</Step.Title>
                </Step.Content>
            </Step>
        </Step.Group>
    )
}

export default observer(WizardSteps);