﻿import React, { useContext, Fragment } from 'react';
import { Container, Segment } from 'semantic-ui-react';
import WizardSteps from './steps/WizardSteps';
import { Route } from 'react-router';
import JavaForm from './forms/JavaForm';
import GitForm from './forms/GitForm';
import JenkinsForm from './forms/JenkinsForm';
import OCPForm from './forms/OCPForm';
import ConsoleStreamPopup from '../Console/ConsoleStreamPopup';
import WizardStore from '../../stores/WizardStore';
import { observer } from 'mobx-react-lite';

const ProjectWizard = () => {

    const wizardStore = useContext(WizardStore)
    const { processing } = wizardStore

    return (
        <Fragment>
            {!processing &&
                <Container style={{ marginTop: '3em' }}>
                    <Segment>
                        <WizardSteps />
                    </Segment>
                    <Segment>
                        <Container>
                            <Route exact path='/projects/form/java' component={JavaForm} />
                            <Route exact path='/projects/form/git' component={GitForm} />
                            <Route exact path='/projects/form/jenkins' component={JenkinsForm} />
                            <Route exact path='/projects/form/ocp' component={OCPForm} />
                        </Container>
                    </Segment>
                </Container>
            }
            <Route exact path='/projects/form/processing' component={ConsoleStreamPopup} />
        </Fragment>
     )
}

export default observer(ProjectWizard);