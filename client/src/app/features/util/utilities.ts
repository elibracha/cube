﻿import { v4 as uuid } from 'uuid';
import Steps from '../../models/Steps';

const genToken = () => {
    return uuid()
}

const goToWizard = (step: number) => {
    switch (step) {
        case Steps.Java:
            return '/projects/form/java'
        case Steps.Git:
            return '/projects/form/git'
        case Steps.Jenkins:
            return '/projects/form/jenkins'
        case Steps.OCP:
            return '/projects/form/ocp'
    }
}

export { genToken, goToWizard }