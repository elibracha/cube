﻿import React, {useContext, useEffect, useState} from 'react'
import { Modal } from 'semantic-ui-react'
import WizardStore from '../../stores/WizardStore'
import { RouteComponentProps } from 'react-router'
import { goToWizard } from '../util/utilities'
import success from './success-animation.gif';

const ConsoleStream: React.FC<{ streamValue: string }> = ({ streamValue }) => (
	<div className="wrapper">
		<div className="cmd">
			<div className="title-bar">poalim@splinter: ~</div>
			<div className="tool-bar">
				<ul>
					<li>File</li>
					<li>Edit</li>
					<li>View</li>
				</ul>
			</div>
			<textarea readOnly className="textarea" value={streamValue}/>
		</div>
	</div>
);

const ConsoleStreamPopup: React.FC<RouteComponentProps> = ({ history }) => {
	const wizardStore = useContext(WizardStore);
	const { processWizard, currentStep, processing } = wizardStore;
	const [ processingStream, setProcessingStream ] = useState({ stream: ''});

	if (!processing) 
		history.push(goToWizard(currentStep)!);

	useEffect(() => {
		const eventSource = new EventSource('http://localhost:8080/weatherstream');
		let stream = '';

		eventSource.onmessage = (event: any) => {
			const data = JSON.parse(event.data).weather;
			stream += JSON.stringify(data) +  '\n';

			setProcessingStream({stream: stream });
			console.log(processingStream)
		};
		eventSource.onerror = (event: any) => console.log('error', event);

	}, [setProcessingStream, processingStream]);

	return (
		<Modal basic open={processing}>
			<Modal.Header>Building In Process ...</Modal.Header>
			<Modal.Content>
				<ConsoleStream streamValue={processingStream.stream}/>
			</Modal.Content>
		</Modal>
	)
};

export default ConsoleStreamPopup;