﻿import React from 'react';
import { Sidebar, Menu, Icon, Container } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

const SideBar = () => (
        <Container style={{ marginTop: '0' }}>
            <Sidebar
                as={Menu}
                direction='left'
                icon='labeled'
                vertical
                visible
                width='thin'>
                <Menu.Item as={Link} exact='true' to=''>
                    <Icon name='folder open' /> Projects
                </Menu.Item>
                <Menu.Item as={Link} exact='true' to=''>
                    <Icon name='history' /> History
                </Menu.Item>
                <Menu.Item as={Link} exact='true' to=''>
                    <Icon name='share square' /> Requests
                </Menu.Item>
                <Menu.Item as={Link} exact='true' to=''>
                    <Icon name='bug' /> Issuses
                </Menu.Item>
                <Menu.Item as={Link} exact='true' to=''>
                    <Icon name='dochub' /> Doc
                </Menu.Item>
                <Menu.Item as={Link} exact='true' to=''
                            style={{ position: 'absolute', bottom: '10px', display: 'blocked', width: '100%' }}>
                    <Icon name='globe' /> Support
                </Menu.Item>
            </Sidebar>
        </Container>
)

export default SideBar;