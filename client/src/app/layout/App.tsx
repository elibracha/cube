﻿import React from 'react';
import SideBar from '../features/Sidebar/SideBar';
import NavBar from '../features/NavBar/NavBar';
import { Grid } from 'semantic-ui-react';
import ProjectWizard from '../features/Wizard/ProjectWizard';

const App: React.FC = () => {
    return (
        <Grid>
            <Grid.Column width={2}>
                <SideBar/>
            </Grid.Column>
            <Grid.Column width={14}>
                <NavBar />
                <ProjectWizard />
            </Grid.Column>
        </Grid>
  );
}

export default App;
