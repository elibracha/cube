﻿import JavaBuildTools from "./JavaBuildTools";

interface IJavaWizardConfig {
    javaVersion: number
    buildTool: JavaBuildTools
    groupId: string
    artifactName: string
    version: string
    description: string
}

export default IJavaWizardConfig;