﻿interface IOCPWizardConfig {
    namespace: string
    owner: string
    groupOwners: string
}

export default IOCPWizardConfig;