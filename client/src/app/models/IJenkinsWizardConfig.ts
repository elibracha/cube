﻿interface IJenkinsWizardConfig {
    url: string
    username: string
    password: string
    jobName: string
}

export default IJenkinsWizardConfig;