﻿interface IGitWizardConfig {
    url: string
    username: string
    password: string
    token: string
    projectName: string
}

export default IGitWizardConfig;