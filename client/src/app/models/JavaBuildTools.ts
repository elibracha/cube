﻿enum JavaBuildTools {
    Maven = 1,
    Gradle,
    Ant
}

export default JavaBuildTools;