﻿enum JavaVersions {
    Java_8 = 0,
    Java_11,
    Java_12
}

export default JavaVersions;