﻿import { observable, action, configure, runInAction } from 'mobx';
import { createContext } from 'react';
import IJavaWizardConfig from '../models/IJavaWizardConfig';
import IGitWizardConfig from '../models/IGitWizardConfig';
import IOCPWizardConfig from '../models/IOCPWizardConfig';
import IJenkinsWizardConfig from '../models/IJenkinsWizardConfig';
import agent from '../agent/agent';

configure({ 'enforceActions': 'always' });

class WizardStore {

    @observable currentJavaWizardConfig: IJavaWizardConfig | null = null;
    @observable currentGitWizardConfig: IGitWizardConfig | null = null;
    @observable currentJenkinsWizardConfig: IJenkinsWizardConfig | null = null;
    @observable currentOcpWizardConfig: IOCPWizardConfig | null = null;

    @observable consoleProcessStreaming = '';
    @observable submittingStep = false;
    @observable currentStep = 0;
    @observable processing = false;

    @action stepBack = () => {
        this.currentStep -= 1
    };

    @action stepForward = () => {
        this.currentStep++
    };

    @action setJavaWizardConfig = async (javaConfig: IJavaWizardConfig) => {
        this.submittingStep = true;

        try {
            this.currentJavaWizardConfig = javaConfig;
                
             this.stepForward()
         } catch (error) {
             console.log(error)
         } finally {
                runInAction(() => this.submittingStep = false)
         }
    };

    @action setGitWizardConfig = async (gitConfig: IGitWizardConfig) => {
        this.submittingStep = true;

        try {
            this.currentGitWizardConfig = gitConfig;

            this.stepForward()
        } catch (error) {
            console.log(error)
        } finally {
            runInAction(() => this.submittingStep = false)
        }
    };

    @action setJenkinsWizardConfig = async (jenkinsConfig: IJenkinsWizardConfig) => {
        this.submittingStep = true;

        try {
            this.currentJenkinsWizardConfig = jenkinsConfig;

            this.stepForward()
        } catch (error) {
            console.log(error)
        } finally {
            runInAction(() => this.submittingStep = false)
        }
    };

    @action setOcpWizardConfig = async (ocpConfig: IOCPWizardConfig) => {
        this.submittingStep = true;

        try {
            this.currentOcpWizardConfig = ocpConfig;
            this.processing = true
        } catch (error) {
            console.log(error)
        } finally {
            runInAction(() => this.submittingStep = false)
        }
    };

    @action processWizard = async () => {
        try {
           // this.processing = true
            await agent.get('weatherstream')
        } catch (error) {
            console.log(error)
        } finally {
            runInAction(() => null)
        }
    };

}

export default createContext(new WizardStore())